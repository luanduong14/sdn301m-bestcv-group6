import SimpleTemplate from "./CVTemplate/SimpleTemplate";
import Toolbar from "./Components/Toolbar";
function App() {
  return (
    <>
       <Toolbar />
       <SimpleTemplate/>
    </>
  );
}

export default App;
